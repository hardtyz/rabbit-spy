.PHONY: build build-linux build-windows build-mac build-mac-arm clean gomodgen

NAME := rabbit-spy
VERSION := $(shell git describe --tags --abbrev=0)
REVISION := $(shell git rev-parse --short HEAD)
LDFLAGS := -X 'main.version=$(VERSION)' \
           -X 'main.revision=$(REVISION)'
GOIMPORTS ?= goimports
GOCILINT ?= golangci-lint
GO ?= GO111MODULE=on go
.DEFAULT_GOAL := help

#build: clean gomodgen build-linux build-windows build-mac build-mac-arm

build-linux: clean gomodgen
	export GO111MODULE=on
	env GOOS=linux go build -ldflags="-s -w -X main.Version=${CI_PIPELINE_IID}" -o bin/linux/rabbit-spy ./

build-windows: clean gomodgen
	export GO111MODULE=on
	env GOOS=windows go build -ldflags="-s -w -X main.Version=${CI_PIPELINE_IID}" -o bin/win/rabbit-spy.exe ./

build-mac: clean gomodgen
	export GO111MODULE=on
	env GOOS="darwin" go build -ldflags="-s -w -X main.Version=${CI_PIPELINE_IID}" -o bin/mac/rabbit-spy ./


build-mac-arm: clean gomodgen
	export GO111MODULE=on
	env GOOS="darwin" GOARCH=arm64 go build -ldflags="-s -w -X main.Version=${CI_PIPELINE_IID}" -o bin/mac-arm/rabbit-spy ./

s3update-linux: build-linux
	aws s3 cp bin/linux/rabbit-spy s3://goparrot-release-utils/rabbit-spy/rabbit-spy-linux-amd64 --acl public-read

s3update-mac: build-mac
	aws s3 cp bin/mac/rabbit-spy s3://goparrot-release-utils/rabbit-spy/rabbit-spy-darwin-amd64 --acl public-read

s3update-mac-arm: build-mac-arm
	aws s3 cp bin/mac-arm/rabbit-spy s3://goparrot-release-utils/rabbit-spy/rabbit-spy-darwin-arm64 --acl public-read

s3updateAll: s3update-linux s3update-mac s3update-mac-arm s3updateVersion

s3updateVersion:
	echo -n ${CI_PIPELINE_IID} > VERSION && aws s3 cp VERSION  s3://goparrot-release-utils/rabbit-spy/VERSION --acl public-read

clean:
	rm -rf bin

gomodgen:
	go mod tidy
	go mod vendor

build: main.go   ## Build a binary.
	$(GO) build -ldflags "$(LDFLAGS)"

cross: main.go  ## Build binaries for cross platform.
	mkdir -p pkg
	@# darwin
	@for arch in "amd64" "arm64"; do \
		GOOS=darwin GOARCH=$${arch} make build; \
		zip pkg/rabbit-spy_darwin_$${arch}.zip rabbit-spy; \
	done;

upload: clean cross
	aws s3 cp pkg/ s3://goparrot-release-utils/rabbit-spy/ --acl public-read --recursive
