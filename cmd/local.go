/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"log"
	"rabbit-spy/packages/rabbitmq"
	"time"

	"github.com/spf13/cobra"
)

// localCmd represents the local command
var localCmd = &cobra.Command{
	Use:   "local",
	Short: "Connect to local RabbitMQ",
	Long:  `Connect to local RabbitMQ`,
	Run:   localStart,
}

func init() {
	rootCmd.AddCommand(localCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// localCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// localCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func localStart(cmd *cobra.Command, args []string) {
	rc := rabbitmq.RabbitConfig{
		Schema:         "amqp",
		Username:       "guest",
		Password:       "guest",
		Host:           "localhost",
		Port:           "5672",
		ConnectionName: "rabbit-debug",
	}
	rbt := rabbitmq.NewRabbit(rc)
	if err := rbt.Connect(); err != nil {
		log.Fatalln("unable to connect to rabbit", err)
	}
	//

	// Consumer
	cc := rabbitmq.ConsumerConfig{
		ExchangeName:  platform,
		ExchangeType:  "topic",
		RoutingKey:    "#",
		QueueName:     fmt.Sprintf("interceptor-%s", platform),
		ConsumerName:  "debug-tool",
		ConsumerCount: 1,
		PrefetchCount: 1,
	}
	cc.Reconnect.MaxAttempt = 60
	cc.Reconnect.Interval = 1 * time.Second
	csm := rabbitmq.NewConsumer(cc, rbt)
	if err := csm.Start(); err != nil {
		log.Fatalln("unable to start consumer", err)
	}

	select {}
}
