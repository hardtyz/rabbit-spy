/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"rabbit-spy/packages/rabbitmq"
	"strconv"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var cfgFile string
var namespace string
var platform string
var kubeconfig string
var clientset *kubernetes.Clientset

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "rabbit-spy",
	Short: "RabbitMQ Consumer K8S",
	Long:  `RabbitMQ Consumer with auto connection to k8s rabbit mq. Only for k8s dev envs`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: start,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.rabbit-debug.yaml)")
	rootCmd.Flags().StringVarP(&namespace, "namespace", "n", "", "K8s namespace")
	rootCmd.PersistentFlags().StringVarP(&platform, "platform", "p", "", "platform name for listen")
	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func start(cmd *cobra.Command, args []string) {

	if home := homedir.HomeDir(); home != "" {
		kubeconfig = filepath.Join(home, ".kube", "config")
	}

	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	clientset, err = kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	rc := rabbitMqCredentials()
	rbt := rabbitmq.NewRabbit(*rc)
	if err := rbt.Connect(); err != nil {
		log.Fatalln("unable to connect to rabbit", err)
	}
	//

	// Consumer
	cc := rabbitmq.ConsumerConfig{
		ExchangeName:  platform,
		ExchangeType:  "topic",
		RoutingKey:    "#",
		QueueName:     fmt.Sprintf("interceptor-%s", platform),
		ConsumerName:  "debug-tool",
		ConsumerCount: 1,
		PrefetchCount: 1,
	}
	cc.Reconnect.MaxAttempt = 60
	cc.Reconnect.Interval = 1 * time.Second
	csm := rabbitmq.NewConsumer(cc, rbt)
	if err := csm.Start(); err != nil {
		log.Fatalln("unable to start consumer", err)
	}

	select {}
}

func getK8SHost() string {

	nodes, err := clientset.CoreV1().Nodes().List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err.Error())
	}
	for _, item := range nodes.Items {
		for _, address := range item.Status.Addresses {
			if address.Type == "ExternalIP" {
				return address.Address
			}
		}
	}
	log.Print("Not Found Cluster External IP")
	os.Exit(1)
	return ""
}

func rabbitMqPort() string {
	service, err := clientset.CoreV1().Services(namespace).Get(context.TODO(), "rabbitmq-outside", metav1.GetOptions{})
	if err != nil {
		log.Print("Not Found rabbitmq-outside service")
		os.Exit(1)
	}
	return strconv.Itoa(int(service.Spec.Ports[0].NodePort))
}

func rabbitMqCredentials() *rabbitmq.RabbitConfig {
	secret, err := clientset.CoreV1().Secrets(namespace).Get(context.TODO(), "rmq", metav1.GetOptions{})
	if err != nil {
		log.Print("Not Found rabbit credentials")
		os.Exit(1)
	}
	rc := rabbitmq.RabbitConfig{
		Schema:         "amqp",
		Username:       "guest",
		Password:       "guest",
		Host:           getK8SHost(),
		Port:           rabbitMqPort(),
		ConnectionName: "rabbit-debug",
	}
	for s, bytes := range secret.Data {

		switch s {
		case "RMQ_PASSWORD":
			rc.Password = string(bytes)
			break

		case "RMQ_USER":
			rc.Username = string(bytes)
			break

		case "RMQ_VHOST":
			rc.VHost = string(bytes)
			break
		}

	}
	return &rc
}
